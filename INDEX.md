# find

Display all lines in one or more files that contain a given string. Inverse and case-insensitive search possible as well.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FIND.LSM

<table>
<tr><td>title</td><td>find</td></tr>
<tr><td>version</td><td>3.0b LFN (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-07-03</td></tr>
<tr><td>description</td><td>Display all lines in one or more files that contain a given string. Inverse and case-insensitive search possible as well.</td></tr>
<tr><td>keywords</td><td>freedos, find, grep</td></tr>
<tr><td>author</td><td>Jim Hall &lt;jhall -at- freedos.org&gt;</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/</td></tr>
<tr><td>platforms</td><td>DOS (e.g. Borland C, Turbo C), includes own KITTEN library version</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Find</td></tr>
</table>
